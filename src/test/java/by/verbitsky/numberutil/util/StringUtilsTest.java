package by.verbitsky.numberutil.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilsTest {

    @Test
    void isPositiveNumberPositive() {
        boolean expected = true;
        boolean actual = StringUtils.isPositiveNumber("5");
        assertEquals(expected, actual);
    }

    @Test
    void isPositiveNumberNegativeWrongStr() {
        boolean expected = false;
        boolean actual = StringUtils.isPositiveNumber("x52c");
        assertEquals(expected, actual);
    }

    @Test
    void isPositiveNumberNegativeNumber() {
        boolean expected = false;
        boolean actual = StringUtils.isPositiveNumber("-1");
        assertEquals(expected, actual);
    }

    @Test
    void isPositiveNumberNegativeNullArg() {
        boolean expected = false;
        boolean actual = StringUtils.isPositiveNumber(null);
        assertEquals(expected, actual);
    }
}